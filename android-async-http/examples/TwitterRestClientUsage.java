import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;

class TwitterRestClientUsage {
    public void getPublicTimeline() {
        TwitterRestClient.get("statuses/public_timeline.json", null, new JsonHttpResponseHandler() {
        	
        	@Override
        	public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
        		 try {
                     JSONObject firstEvent = (JSONObject) response.get(0);
                     String tweetText = firstEvent.getString("text");

                     // Do something with the response
                     System.out.println(tweetText);
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
        	}
        	
        });
    }
}